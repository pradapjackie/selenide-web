plugins {
    id("java")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("junit:junit:4.13.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    testImplementation("junit:junit:4.13.2")
    implementation("com.codeborne:selenide:6.11.0")
    implementation("com.codeborne:selenide-appium:2.5.2")

}


    tasks.getByName<Test>("test") {
    useJUnitPlatform()
        testLogging.showStandardStreams = true
}



