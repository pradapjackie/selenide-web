package Pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class DashboardScreen {

    public DashboardScreen burger_Menu_Icon_Is_Visible() {
        $(By.id("react-burger-menu-btn")).shouldBe(Condition.visible);
        return page(DashboardScreen.class);
    }
}
