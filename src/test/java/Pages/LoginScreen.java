package Pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class LoginScreen {

    public LoginScreen set_Username(String username) {
        $(By.id("user-name")).setValue(username);
        return page(LoginScreen.class);
    }

    public LoginScreen set_Password(String password){
        $(By.id("password")).setValue(password);
        return  page(LoginScreen.class);
    }

    public LoginScreen click_On_Login(){
        $(By.id("login-button")).click();
        return  page(LoginScreen.class);
    }

    public LoginScreen locked_Out_Error_Message_Is_Visible() {
        $(By.xpath("//div[@class='error-message-container error']")).shouldBe(Condition.visible);
        return page(LoginScreen.class);
    }


}
