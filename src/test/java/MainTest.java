import Pages.DashboardScreen;
import Pages.LoginScreen;
import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.chrome.ChromeOptions;

import static com.codeborne.selenide.Selenide.open;


public class MainTest {

    @BeforeAll
            public static void setUp()
    {
        ChromeOptions options = new ChromeOptions()
                .setHeadless(true)
                .addArguments("--lang=en_US");
        Configuration.browserCapabilities = options;
    }


    LoginScreen loginScreenFunction;
    DashboardScreen dashboardScreen;

    @Test
    public void valid_Login_with_TC_1() throws InterruptedException {
        dashboardScreen = new DashboardScreen();
         loginScreenFunction= open("https://www.saucedemo.com/", LoginScreen.class);
        loginScreenFunction.set_Username("standard_user");
        loginScreenFunction.set_Password("secret_sauce");
        loginScreenFunction.click_On_Login();
        dashboardScreen.burger_Menu_Icon_Is_Visible();
    }

    @Test
    public void valid_Login_with_TC_2() throws InterruptedException {
        dashboardScreen = new DashboardScreen();
        loginScreenFunction= open("https://www.saucedemo.com/", LoginScreen.class);
        loginScreenFunction.set_Username("locked_out_user");
        loginScreenFunction.set_Password("secret_sauce");
        loginScreenFunction.click_On_Login();
        loginScreenFunction.locked_Out_Error_Message_Is_Visible();
    }
    @Test
    public void valid_Login_with_TC_3() throws InterruptedException {
        dashboardScreen = new DashboardScreen();
        loginScreenFunction= open("https://www.saucedemo.com/", LoginScreen.class);
        loginScreenFunction.set_Username("problem_user");
        loginScreenFunction.set_Password("secret_sauce");
        loginScreenFunction.click_On_Login();
        dashboardScreen.burger_Menu_Icon_Is_Visible();
    }
    @Test
    public void valid_Login_with_TC_4() throws InterruptedException {
        dashboardScreen = new DashboardScreen();
        loginScreenFunction= open("https://www.saucedemo.com/", LoginScreen.class);
        loginScreenFunction.set_Username("performance_glitch_user");
        loginScreenFunction.set_Password("secret_sauce");
        loginScreenFunction.click_On_Login();
        dashboardScreen.burger_Menu_Icon_Is_Visible();
    }
}
